unit math;

{$mode tp}

interface
    uses consts;
    
    function root(f : TFunc; g : TFunc; a : real; b : real) : real;
    function integrate(f : TFunc; a : real; b : real) : real;

implementation
    
    function root(f : TFunc; g : TFunc; a : real; b : real) : real;
        function simple(x : real) : real;
        begin
            simple := f(x) - g(x);
        end;
    var
        result : real;
    begin
        repeat
            result := (a + b) / 2;

            if simple(result) * simple(b) = 0 then
            begin
                a := result;
                b := result;
            end
            else if simple(result) * simple(b) < 0 then
            begin
                a := result;
            end
            else
            begin
                b := result;
            end;

            write('.');
        until abs(a - b) < eps1;

        root := result;
    end;

    function integrate(f : TFunc; a : real; b : real) : real;
    var
        i, n : integer;
        prev_sum, sum, h : real;
    begin
        n := 1;
        sum := 0.0;
        prev_sum := 0.0;

        repeat
            prev_sum := sum;
            h := (b - a) / n;
            sum := 0;

            for i := 1 to n + 1 do
            begin
                sum := sum + 0.5 * h * ( f(a + i * h) + f(a + (i - 1) * h) );
            end;

            n := n * 2;

            write('.');

        until abs(sum - prev_sum) < eps2;

        integrate := sum;
    end;
end.
