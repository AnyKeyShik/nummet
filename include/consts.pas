unit consts;

{$mode tp}

interface
    const 
        SEP_LEN  = 35;
        eps1 = 0.001;
        eps2 = 0.001;

    type
        TFunc = function(x : real) : real;

    procedure print_curves;
    procedure print_methods;
    procedure print_sep;

    function f1(x : real) : real;
    function f2(x : real) : real;
    function f3(x : real) : real;

var
    i : integer;
implementation
    procedure print_curves;
    begin
        writeln('f1 = ln(x)');
        writeln('f2 = -2x + 14');
        writeln('f3 = 1/(2-x) + 6');
    end;

    procedure print_methods;
    begin
        writeln('Method for calculate roots: Bisection method');
        writeln('Method for calculate integral: Riemann sum');
    end;

    procedure print_sep;
    begin
        for i := 1 to SEP_LEN do 
            write('-');
        writeln();
    end;

    function f1(x : real) : real;
    begin
        f1 := ln(x);
    end;

    function f2(x : real) : real;
    begin
        f2 := (-2 * x) + 14;
    end;

    function f3(x : real) : real;
    begin
        f3 := (1 / (2 - x )) + 6;
    end;
end.
