Program NumMet;

{$mode tp}

uses crt, consts, math;

var
    x1, x2, x3, square : real;

begin
    clrscr;

    writeln('Welcome to the Square Calculator!');

    print_sep();

    writeln('My curves:');
    print_curves();
    
    print_sep();

    writeln('My methods:');
    print_methods();
   
    print_sep();

    writeln('Calculation root of the equation with accuracy:', eps1:7:3);
    writeln('Starting');
    write('Progress: [');
    x1 := root(f1, f3, 2.1, 2.2);
    x2 := root(f2, f3, 4.2, 4.3);
    x3 := root(f1, f2, 6.1, 6.2);
    writeln(']');
    writeln('Done!');

    print_sep();

    writeln('Roots of equation:');
    writeln('f1 with f3:', x1:7:3);
    writeln('f2 with f3:', x2:7:3);
    writeln('f1 with f2:', x3:7:3);

    print_sep();

    writeln('Calculation integral with accuracy:', eps2:7:3);
    writeln('Starting');
    write('Progress: [');
    square := integrate(f1, x1, x3) + integrate(f3, x1, x2) - integrate(f2, x2, x3);
    writeln(']');
    writeln('Done!');

    print_sep();

    writeln('Square of figure:', square:7:3);
end.
