src := main.pas
target := num_met

target_dir := build
obj_dir := obj
include_dir := include

.PHONY: all, clean, run

all: $(target)

clean:
	@echo "Cleaning all..."
	@rm -rf build obj 2> /dev/null

run: $(target)
	@echo "Run $<..."
	@$(target_dir)/$(target)

$(target): $(src)	
	@mkdir -p $(target_dir)
	@mkdir -p $(obj_dir)
	@fpc -FE$(target_dir) -Fu$(include_dir) -Fo$(obj_dir) -FU$(obj_dir) $(src) -o$(target)
	@rm -r $(obj_dir)
